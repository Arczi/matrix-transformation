const handleInput = require("./matrixTrans").handleInput;
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.log("Type number of tests cases:");
rl.on("line", (input) => handleInput(input,rl));

const transformInput = input => input.split(" ").map(val => parseInt(val));
const calculate = input => {
  const output = [...input];
  const trueArray = [];
  input.forEach((arr, i) => {
    arr.forEach((val, j) => {
      if (val) {
        trueArray.push([i, j]);
      }
    });
  });

  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[i].length; j++) {
      output[i][j] = input[i][j] ? 0 : getClosestDistance([i, j], trueArray);
    }
  }
  return output;
};
module.exports.calculate = calculate;

const getClosestDistance = (pos, trueArray) => {
  let distance = Infinity;
  for (pos1 of trueArray) {
    let newDistance = Math.abs(pos[0] - pos1[0]) + Math.abs(pos[1] - pos1[1]);
    distance = newDistance < distance ? newDistance : distance;
  }

  if(distance === 1){
    return 1;
  }
  

  return distance;
};

const compute = arrays => {
  const computed = [];
  for (let array of arrays) {
    computed.push(calculate(array));
  }

  return computed;
};

const printOutput = arrays => {
  compute(arrays).forEach((array, i) => {
    console.log("Test results nr " + (1 + i) + " :");
    array.forEach(row => {
      console.log(row.join(" "));
    });
  });
};

let n = 0;
let m = 0;
let t = 0;
let nLine = 1;
let nTest = 0;
let bitMap = [];

module.exports.handleInput = (input, rl) => {
  if (nLine === 1) {
    t = parseInt(input);
    if (t >= 1 && t <= 1000) {
      bitMap = new Array(t);
    } else {
      console.log("Number of tests cases t should be 1 >= t <= 1000! Try again");
      return false;
    }
  }

  if (nLine === 2) {
    bitMap[nTest] = [];
    var bitmapDimensions = transformInput(input);
    n = bitmapDimensions[0];
    m = bitmapDimensions[1];

    if (!(n >= 1 && n <= 182 && m >= 1 && m <= 182)) {
      console.log("Cols and rows n,m number should be >=1 and <= 182! Try again");
      return false;
    }
  }

  if (nLine > 2) {
    bitMap[nTest][nLine - 3] = transformInput(input);
    if (!checkBitmapInput(bitMap[nTest][nLine - 3], m)) {
      console.log("Incorrect input! Try again!");
      return false;
    }
  }

  if (nLine > 2 && nLine >= n + 2) {
    nTest++;
    nLine = 2;
    if (nTest >= t) {
      printOutput(bitMap);
      if (!rl) {
        return true;
      }

      rl.close();
      return true;
    } else {
      bitMap[nTest] = [];
    }
  } else {
    nLine++;
  }

  if (nLine === 2) {
    console.log("Type " + (nTest + 1) + " bitmap dimensions:");
  } else if (nLine > 2 && nLine - 2 <= n) {
    console.log("Type " + (nLine - 2) + " row");
  }

  return true;
};

const checkBitmapInput = (array, m) => array.filter(val => val === 0 || val === 1).length === m;

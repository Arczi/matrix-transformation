# Matrix transformation function
## Installation
```npm install```
## Usage
``` npm run ```

## Test
``` npm test ```

## Algorithm 1 - branch master
The program iterates over the matrix and pushes positions of all '1' items to the helper array. 
Then it iterates again over the matrix and search for a closest element in the helper array by iterating over it.

## Algorithm 2 - branch algorithm2
The program iterates over the matrix only once. The algorithm extends the searching area by incrementing the radius until it find first '1'. The area is described with function.

![algorithm](pic1.png "algorithm")

const calculate = require("../matrixTrans").calculate;
const handleInput = require("../matrixTrans").handleInput;

test("calculate test", () => {
  //Given
  const testedArray = [[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 0]];
  const expected = [[3, 2, 1, 0], [2, 1, 0, 0], [1, 0, 0, 1]];

  //Then
  expect(calculate(testedArray)).toStrictEqual(expected);
});

test("print test", () => {
  //Then
  expect(handleInput("1")).toBeTruthy();
  expect(handleInput("2 2")).toBeTruthy();
  expect(handleInput("2 2")).toBeFalsy();
  expect(handleInput("1 1")).toBeTruthy();
  expect(handleInput("1 1")).toBeTruthy();
});
